## [1.3.7](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.6...1.3.7) (2024-05-15)


### :bug: Fixes

* add rules to skip job if CHANGELOG has changed ([598dbbd](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/598dbbd29dd896697a03737e9d2500b878e0ab72)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.6](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.5...1.3.6) (2024-05-15)


### :bug: Fixes

* remove skip ci for CHANGELOG ([882ce43](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/882ce437220a802ddad653ec8d6cdca4eda17c48)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.5](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.4...1.3.5) (2024-05-15)


### :bug: Fixes

* skip ci for CHANGELOG ([126f087](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/126f087e8ce6473981b80a6113538d4ea80f6316)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.4](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.3...1.3.4) (2024-05-15)


### :bug: Fixes

* skip ci for CHANGELOG ([766f6c5](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/766f6c5cc613fa1f6b40a3f0d88a68b5f86efd5d)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.3](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.2...1.3.3) (2024-05-15)


### :bug: Fixes

* skip ci for CHANGELOG ([631c363](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/631c36300fe8bad8b325cd2cc677476ec1278011)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.2](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.1...1.3.2) (2024-05-15)


### :repeat: Chores

* test if new release config will work without hardcoded url ([b358f60](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/b358f60adb3ef0e8cf60b5a785bcc09720738f61)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.1](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.3.0...1.3.1) (2024-05-15)


### :bug: Fixes

* flexible project path ([41d804a](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/41d804a01e447a2ff246d854a8171c337f69bd14)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.3.0](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.2.4...1.3.0) (2024-05-15)


### :sparkles: Features

* disable semver in mr ([eb769d8](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/eb769d81ddd5a9f211b3804d7e00367c10fd5f53)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.2.4](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.2.3...1.2.4) (2024-05-15)


### :bug: Fixes

* update release tools ([5a1c43a](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/5a1c43a5b85ed728e57272f4e6e8a3059f920a3c)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.2.3](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.2.2...1.2.3) (2024-05-15)


### :bug: Fixes

* update release tools ([42ec325](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/42ec325ec3a5333d160ace1b76716d94b5779d6a)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)

## [1.2.2](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/compare/1.2.1...1.2.2) (2024-05-15)


### :bug: Fixes

* install @semantic-release/changelog ([da0ff92](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/da0ff92c65cba4f2063f4ca870192e280cbca5d9)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)
* install @semantic-release/changelog ([16fffc8](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/16fffc8e09b518b302892a1a96b814e17867e71c)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)
* install @semantic-release/changelog ([dcb769f](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/dcb769f531f0c6aaed2d948b711339b5ad9f6f06)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)
* update release components ([85c2e21](https://gitlab.moselwal.io/devops/ci-cd-components/release-tools/commit/85c2e21525ccbb375e95360d9713007afb86bcd4)), closes [no-issue#0](https://gitlab.moselwal.io/devops/no-issue/issues/0)
